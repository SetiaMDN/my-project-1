from django.urls import path
from .views import home, misc

urlpatterns = [
    path('', home, name="Home"),
    path('misc/', misc, name="Misc"),
]